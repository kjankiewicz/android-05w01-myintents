package com.example.kjankiewicz.android_05w01_myintents

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

class SecondActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val intent = this.intent
        val mEditTextValue = intent.getIntExtra(MainActivity.EXTRA_INTDATA, 0)

        if (mEditTextValue != 0) {
            val conData = Bundle()
            conData.putInt("squared_result", mEditTextValue * mEditTextValue)
            val returnIntent = Intent()
            returnIntent.putExtras(conData)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }
    }

    /*EditText editText = (EditText) findViewById(R.id.second_activity_edit_text);
    editText.setText(String.valueOf(mEditTextValue));*/

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.second, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }
}
