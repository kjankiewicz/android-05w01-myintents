package com.example.kjankiewicz.android_05w01_myintents

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText


class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button: Button = findViewById(R.id.explicit_activity_button)

        button.setOnClickListener {
            val explicitIntent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(explicitIntent)
        }

        button = findViewById(R.id.explicit_activity_button_with_data)
        button.setOnClickListener {
            val explicitIntent = Intent(this@MainActivity, SecondActivity::class.java)
            val editText = findViewById<EditText>(R.id.editText_for_explicit_activity)
            val editTextVal = Integer.parseInt(editText.text.toString())
            explicitIntent.putExtra(EXTRA_INTDATA, editTextVal)
            startActivityForResult(explicitIntent, GET_SQUARED_REQUEST)
        }

        button = findViewById(R.id.explicit_service_button_with_data)
        button.setOnClickListener {
            val explicitIntent = Intent()
            explicitIntent.setClassName(
                    this@MainActivity,
                    "com.example.kjankiewicz.android_05w01_myintents.MyDummyService")
            explicitIntent.putExtra(EXTRA_INTDATA, 6)
            startService(explicitIntent)
        }

        button = findViewById(R.id.implicit_web_browser_button)
        button.setOnClickListener {
            val myPage = Uri.parse("http://jankiewicz.pl/studenci/panum.html")
            var implicitIntent = Intent(Intent.ACTION_VIEW, myPage)
            if (implicitIntent.resolveActivity(packageManager) != null) {
                implicitIntent = Intent.createChooser(implicitIntent, "Otwórz stronę za pomocą:")
                startActivity(implicitIntent)
            }
        }


        button = findViewById(R.id.implicit_sms_client_button)
        button.setOnClickListener {
            val implicitIntent = Intent(Intent.ACTION_SEND)
            implicitIntent.putExtra(Intent.EXTRA_TEXT, "wiadomość")
            implicitIntent.type = "text/plain"
            if (implicitIntent.resolveActivity(packageManager) != null) {
                startActivity(implicitIntent)
            }
        }

        button = findViewById(R.id.implicit_do_all_activity_button)
        button.setOnClickListener {
            val implicitIntent = Intent()
            implicitIntent.action = "com.example.kjankiewicz.android_05w02_myalldoapp.DOALL"
            if (implicitIntent.resolveActivity(packageManager) != null) {
                startActivity(implicitIntent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int,
                                  resultCode: Int,
                                  data: Intent) {
        if (resultCode == Activity.RESULT_OK && requestCode == GET_SQUARED_REQUEST) {
            val res = data.extras
            var result = 0
            if (res != null) {
                result = res.getInt("squared_result")
            }
            val editText = findViewById<EditText>(
                    R.id.editText_for_explicit_activity)
            editText.setText(result.toString())
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_INTDATA = "com.example.kjankiewicz.android_05w01_myintents.INTDATA"
        private const val GET_SQUARED_REQUEST = 0
    }
}
